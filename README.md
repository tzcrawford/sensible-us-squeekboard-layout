# Sensible Squeekboard Layout for US English

The default keyboard layout for US english is subjectively poor for typing speed at least while working on a cli.
This layout attempts to remedy the problem.
Keys have been moved around, added to particular menus, and button elements resized so that it fits the screen properly in both portrait and landscape orientation.

# Installation

The included files can simply be copied to `~/.local/share/squeekboard/`. 
This will replace the default layout.
To revert back to the default layout, simply remove these files.

Alternatively you can go to the "Region & Language" settings in phosh and enable the "A user-defined custom Layout" input source (under Other). 
If you run `gsettings get org.gnome.desktop.input-sources sources` you will get `[('xkb', 'custom')]`. 
So rename the `us.yaml` files to `custom.yaml` and `us_wide.yaml` to `custom_wide.yaml` etc.
You can then switch input sources with the preferences (globe) button on the keyboard.
